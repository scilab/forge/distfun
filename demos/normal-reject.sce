// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// Reference
// Karl Sigman. Acceptance-rejection method, 2007. 
// Professor Karl Sigman’s Lecture Notes on Monte Carlo Simulation, 
// Columbia University, 
// http://www.columbia.edu/~ks20/4703-Sigman/4703-07-Notes-ARM.pdf.

// Random Number Generation
// Rejection method
// http://dept.stat.lsa.umich.edu/~kshedden/Courses/Stat606/Notes/random.pdf
// Kerby Shedden
// Statistics 606: Computational Statistics

function demonormalreject()

    mprintf("Example of rejection sampling :");
    mprintf("See how to generate standard normal random variables, ");
    mprintf("using a rejection algorithm ");
    mprintf("and a instrumental Cauchy distribution.");

    function x=mycauchyrnd(n)
        // Simulate n standard Cauchy random variables
        // Note : The standard Cauchy distribution is equal to 
        // the Student's t-distribution with one degree of freedom.
        // So we could use distfun_trnd. 
        // Here we use a simulation approach, to make clearer 
        // that this is possible. 
        u=distfun_unifrnd(0,1,[n 1])
        x=tan(%pi*(u-0.5))
    endfunction

    // 1. Check the Cauchy generator
    n=1000;
    data=mycauchyrnd(n);
    scf();
    subplot(2,2,1)
    x=linspace(-5,5,20);
    histplot(x,data);
    x=linspace(-5,5);
    y=distfun_tpdf(x,1);
    plot(x,y)
    xtitle("Cauchy distribution","X","Density")

    // 2. Check the distributions
    x=linspace(-3,3);
    y=distfun_normpdf(x,0,1);
    subplot(2,2,2)
    plot(x,y,"r-")
    c = sqrt(2*%pi/%e);
    y=distfun_tpdf(x,1);
    plot(x,c*y,"b-")
    legend(["Normal(0,1)","c*Cauchy"])
    stitle=msprintf("Rejection algorithm, c=%.2f",c);
    xtitle(stitle,"X","Density")

    // 3. Check the rejection algorithm
    function x=mynormrejectrnd()
        c = sqrt(2*%pi/%e);
        while (%t)
            y=mycauchyrnd(1)
            u=distfun_unifrnd(0,1)
            f=distfun_normpdf(y,0,1)
            g=distfun_tpdf(y,1)
            if (u<f/c/g) then
                x=y
                break
            end
        end
    endfunction

    R=100;
    for i=1:R
        z(i)=mynormrejectrnd();
    end
    subplot(2,2,3)
    x=linspace(-3,3,20);
    histplot(x,z);
    x=linspace(-3,3);
    y=distfun_normpdf(x,0,1);
    plot(x,y);
    legend(["Data","Normal(0,1)"]);
    xtitle("Rejection algorithm","X","Density")

    // 4. See why f(x)/g(x) <= sqrt(2*%pi/%e)
    x=linspace(-5,5);
    // Compute q(x)=f(x)/g(x)
    q=sqrt(%pi/2)*(1+x.^2).*exp(-0.5*x.^2);
    subplot(2,2,4)
    plot(x,q);
    title("Ratio f/g");
    xlabel("x")
    c=sqrt(2*%pi/%e);
    plot(x,c*ones(x),"r-")
    legend(["f(x)/g(x)","sqrt(2pi/e)"]);

    //
    // Load this script into the editor
    //
    filename = "normal-reject.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction

demonormalreject();
clear demonormalreject
