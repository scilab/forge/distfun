// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
path = distfun_getpath (  );
exec(fullfile(path,"tests","unit_tests","testingutilities.sce"));
//
// Consistency Checks
//
a=4;
b=6;
x=[
    4.2  
    4.6  
    5.   
    5.4  
    5.8  
];
CheckPDF("distfun_unifpdf",list(distfun_unifpdf,a,b),x);
CheckPDF: Checking empty matrix...
CheckPDF: y=distfun_unifpdf([],[],[])
CheckPDF: ... OK
CheckPDF: Get reference quantiles...
CheckPDF: y(1)=distfun_unifpdf(4.2,4,6)
CheckPDF: y(2)=distfun_unifpdf(4.6,4,6)
CheckPDF: y(3)=distfun_unifpdf(5,4,6)
CheckPDF: y(4)=distfun_unifpdf(5.4,4,6)
CheckPDF: y(5)=distfun_unifpdf(5.8,4,6)
CheckPDF: ... OK
CheckPDF: With arguments expanded...
CheckPDF: y=distfun_unifpdf([4.2;4.6;5;5.4;5.8],4,6)
CheckPDF: y=distfun_unifpdf([4.2;4.6;5;5.4;5.8],4,[6;6;6;6;6])
CheckPDF: y=distfun_unifpdf([4.2;4.6;5;5.4;5.8],[4;4;4;4;4],6)
CheckPDF: y=distfun_unifpdf([4.2;4.6;5;5.4;5.8],[4;4;4;4;4],[6;6;6;6;6])
CheckPDF: ... OK
rtol=1.e-9;
CheckPDFvsCDF(list(distfun_unifpdf,a,b),list(distfun_unifcdf,a,b),x,rtol);
CheckPDFvsCDF: Get reference PDF...
CheckPDFvsCDF: Derivate the CDF...
CheckPDFvsCDF: Check...
CheckPDFvsCDF: ...OK
//
// Test the accuracy
//
path=distfun_getpath();
dataset = fullfile(path,"tests","unit_tests","unif","uniform.dataset.csv");
table = readCsvDataset ( dataset );
precision = 10*%eps;
ntests = size(table,"r");
for k = 1 : ntests
    x = table(k,1);
    a = table(k,2);
    b = table(k,3);
    y = table(k,4);
    computed = distfun_unifpdf ( x , a , b );
    assert_checkalmostequal ( computed , y , precision );
end
