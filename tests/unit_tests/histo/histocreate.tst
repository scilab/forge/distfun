// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

function checkhisto(h)
    // In general, users should not access to the
    // fields of h. 
    // Anyway, this is simpler, so we do it to do the tests.
    // Checks the properties of an histogram object
    assert_checktrue(h.nbins>0);
    // Check pdf
    assert_checkequal(size(h.pdf),[h.nbins,1]);
    binwidths=h.edges(2:$)-h.edges(1:$-1)
    assert_checkalmostequal(sum(h.pdf.*binwidths),1.,10*%eps);
    assert_checktrue(and(h.pdf>0.));
    assert_checktrue(and(h.pdf<=1.));
    // Check cdf
    assert_checkequal(size(h.cdf),[h.nbins,1]);
    assert_checktrue(and(h.cdf>=0.));
    assert_checktrue(and(h.cdf<=1.));
    // Check edges
    assert_checkequal(size(h.edges),[h.nbins+1,1]);
    assert_checktrue(diff(h.edges)>0);
endfunction

m=1000; // Number of observations
data=distfun_normrnd(0,1,m,1);
//
h=distfun_histocreate("data",data);
checkhisto(h);
//
// Provide the number of bins
nbins=10;
h=distfun_histocreate("data",data,nbins);
checkhisto(h);
assert_checkequal(h.nbins,nbins);
//
// Provide the binmethod
h=distfun_histocreate("data",data,[],"sturges");
checkhisto(h);

//
// A dataset which may create empty classes, 
// if the default setting is used.
data=[
-0.1777307  
-0.2306779  
-0.0169847  
2.4897635  
0.0541673  
-0.9655585  
-0.0975121  
-0.1682635  
-0.0737535  
-0.6739954  
];
h=distfun_histocreate("data",data);
checkhisto(h);
//
// Check integer data
m=1000; // Number of observations
pr=0.7;
data=distfun_geornd(pr,1,m);
h=distfun_histocreate("data",data,[],"integers");
checkhisto(h);
//
// Check integer data with given number of bins
m=1000; // Number of observations
pr=0.7;
data=distfun_geornd(pr,1,m);
nbins=3
h=distfun_histocreate("data",data,nbins,"integers");
checkhisto(h);
assert_checkequal(h.nbins,nbins);
//
// Check integer data with given edges
m=1000; // Number of observations
pr=0.7;
data=distfun_geornd(pr,1,m);
edges=[0,1,2];
h=distfun_histocreate("data",data,edges,"integers");
checkhisto(h);
assert_checkequal(h.nbins,2);
//
// Create histogram from edges and heights
edges=[
  -3.028
  -1.978
  -0.927
   0.122
   1.173
   2.223
   3.273
];
heights=[
    0.0314 
    0.1361 
    0.3541 
    0.3170 
    0.0980 
    0.0152 
];
h=distfun_histocreate("pdf",edges,heights);
checkhisto(h);
//
// Create histogram from edges and heights
edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
heights=[0.0314,0.1361,0.3541,0.317,0.098,0.0152];
h=distfun_histocreate("pdf",edges,heights);
checkhisto(h);
//
// Create histogram from edges and heights
// Empty classes at the begining.
edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
heights=[0.,0.1361,0.3541,0.317,0.098,0.0152];
h=distfun_histocreate("pdf",edges,heights);
checkhisto(h);
//
// Create histogram from edges and heights
// Empty classes at the end.
edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
heights=[0.,0.1361,0.3541,0.317,0.098,0.];
h=distfun_histocreate("pdf",edges,heights);
checkhisto(h);
//
// Create histogram from edges and heights
// Empty classes in the middle.
edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
heights=[0.,0.1361,0.,0.,0.098,0.];
h=distfun_histocreate("pdf",edges,heights);
checkhisto(h);
