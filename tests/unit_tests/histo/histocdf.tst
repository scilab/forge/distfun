// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

m=1000;
data=distfun_normrnd(0,1,m,1);
h=distfun_histocreate("data",data);
//
x=linspace(-4,4);
p=distfun_histocdf(x,h);
pnorm=distfun_normcdf(x,0,1);
// Error measure
score=sum(abs(p-pnorm).^2);
assert_checktrue(score<1);
//
// Check lower tail
p=distfun_histocdf(x,h,%f);
pnorm=distfun_normcdf(x,0,1,%f);
// Error measure
score=sum(abs(p-pnorm).^2);
assert_checktrue(score<1);
//
// Create histogram from edges and heights
edges=[
  -3.0285
  -2.6783
  -2.3282
  -1.9781
  -1.6279
  -1.2778
  -0.9277
  -0.5775
  -0.2274
   0.1226
   0.4728
   0.8229
   1.1730
   1.5232
   1.8733
   2.2234
   2.5736
   2.9237
   3.2738
];
heights=[
    0.00571
    0.03141
    0.05712
    0.07140
    0.11709
    0.21991
    0.31130
    0.33130
    0.41983
    0.35700
    0.33415
    0.25990
    0.11995
    0.11995
    0.05426
    0.02856
    0.01428
    0.00285
];
h=distfun_histocreate("pdf",edges,heights);
x=linspace(-4,4);
p=distfun_histocdf(x,h);
pnorm=distfun_normcdf(x,0,1);
// Error measure
score=sum(abs(p-pnorm).^2);
assert_checktrue(score<1);
