// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

m=1000;
data=distfun_normrnd(0,1,m,1);
h=distfun_histocreate("data",data);
//
x=linspace(-4,4);
y=distfun_histopdf(x,h);
ynorm=distfun_normpdf(x,0,1);
// Error measure
score=sum(abs(y-ynorm).^2);
assert_checktrue(score<0.1);

