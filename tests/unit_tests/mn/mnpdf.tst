// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


nb = 10;
P = [0.3 0.4 0.3];
x = [4 0 6];
p = distfun_mnpdf(x,nb,P);
p_expected = 0.001240029;
assert_checkalmostequal(p,p_expected);
