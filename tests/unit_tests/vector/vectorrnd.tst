// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- NO CHECK ERROR OUTPUT -->

function rho=mycorrcoef(a,b)
    C=cov([a,b]);
    D=sqrt(diag(C)*diag(C)');
    R=C./D
    rho=R(1,2)
endfunction

path = distfun_getpath (  );
exec(fullfile(path,"tests","unit_tests","testingutilities.sce"));

sigma = [
1.0  0.5
0.5  1.0
];
v1=list("unif",5,12); // Uniform(min=5,max=12)
v2=list("norm",-5,7); // Normal(mean=-5,sigma=7)
r=list(v1,v2);
x=distfun_vectorrnd(r,10000,sigma);
// Check correlation coefficient
rho=mycorrcoef(x(:,1),x(:,2));
assert_checkalmostequal(rho,0.5,[],0.05);
// The number of classes in the histogram
// NC must be even.
NC = 2*12;
N=10000;
// Check marginal : Uniform
rtol = 1.e-1;
atol = 1.e-1;
R=x(:,1);
checkRNGLaw ( R , list(distfun_unifcdf,5,12) , N , NC , rtol , atol );
// Check marginal : Normal
rtol = 1.e-1;
atol = 1.e-1;
R=x(:,2);
checkRNGLaw ( R , list(distfun_normcdf,-5,7) , N , NC , rtol , atol );

//
// Graphical "test"
h=scf();
for i=1:4
    subplot(2,2,i)
    rho=(i-1)/4;
    sigma = [1.0  rho;rho  1.0];
    x=distfun_vectorrnd(r,1000,sigma);
    plot(x(:,1),x(:,2),"bo");
    title("rho="+string(rho));
end
delete(h);
//scf();
//subplot(1,2,1)
//histo(x(:,1));
//title("Uniform(5,12)");
//subplot(1,2,2)
//histo(x(:,2));
//title("Normal(-5,7)");
//
// Independent
v1=list("unif",5,12); // Uniform(min=5,max=12)
v2=list("norm",-5,7); // Normal(mean=-5,sigma=7)
r=list(v1,v2);
x=distfun_vectorrnd(r,10000);
// Check correlation coefficient
rho=mycorrcoef(x(:,1),x(:,2));
assert_checkalmostequal(rho,0.,[],0.05);
// The number of classes in the histogram
// NC must be even.
NC = 2*12;
N=10000;
// Check marginal : Uniform
rtol = 1.e-1;
atol = 1.e-1;
R=x(:,1);
checkRNGLaw ( R , list(distfun_unifcdf,5,12) , N , NC , rtol , atol );
// Check marginal : Normal
rtol = 1.e-1;
atol = 1.e-1;
R=x(:,2);
checkRNGLaw ( R , list(distfun_normcdf,-5,7) , N , NC , rtol , atol );
