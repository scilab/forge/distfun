// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
path = distfun_getpath (  );
exec(fullfile(path,"tests","unit_tests","testingutilities.sce"));
//
// Consistency Checks
//
v=5;
x=[
-1.475884   
-0.5594296  
0.         
0.5594296  
1.475884   
];
CheckPDF("distfun_tpdf",list(distfun_tpdf,v),x);
CheckPDF: Checking empty matrix...
CheckPDF: y=distfun_tpdf([],[])
CheckPDF: ... OK
CheckPDF: Get reference quantiles...
CheckPDF: y(1)=distfun_tpdf(-1.475884,5)
CheckPDF: y(2)=distfun_tpdf(-0.5594296,5)
CheckPDF: y(3)=distfun_tpdf(0,5)
CheckPDF: y(4)=distfun_tpdf(0.5594296,5)
CheckPDF: y(5)=distfun_tpdf(1.475884,5)
CheckPDF: ... OK
CheckPDF: With arguments expanded...
CheckPDF: y=distfun_tpdf([-1.475884;-0.5594296;0;0.5594296;1.475884],5)
CheckPDF: y=distfun_tpdf([-1.475884;-0.5594296;0;0.5594296;1.475884],[5;5;5;5;5])
CheckPDF: ... OK
rtol=1.e-9;
CheckPDFvsCDF(list(distfun_tpdf,v),list(distfun_tcdf,v),x,rtol);
CheckPDFvsCDF: Get reference PDF...
CheckPDFvsCDF: Derivate the CDF...
CheckPDFvsCDF: Check...
CheckPDFvsCDF: ...OK
//
// Test accuracy
//
path=distfun_getpath();
dataset = fullfile(path,"tests","unit_tests","t","t.dataset.csv");
table = readCsvDataset(dataset);
precision = 100*%eps;
nt = size(table,"r");
for k = 1 : nt
    x = table(k,1);
    v = table(k,2);
    p = table(k,3);
    computed = distfun_tpdf ( x , v );
    if (%f) then
        d = assert_computedigits ( computed , p );
        mprintf("Test #%d/%d: Digits = %.1f\n",k,nt,d);
    end
    assert_checkalmostequal ( computed , p , precision );
end
