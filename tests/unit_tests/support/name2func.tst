// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// "inv" for p=0.7 of Normal(0,1)
finvnorm=distfun_name2func("inv","norm");
mu=0; 
sigma=1;
x=finvnorm(0.7,mu,sigma);
x2=distfun_norminv(0.7,mu,sigma);
assert_checkequal(x,x2);
//
// "pdf" for x=2.1 of Normal(0,1)
fpdfnorm=distfun_name2func("pdf","norm");
mu=0; 
sigma=1;
y=fpdfnorm(2.1,mu,sigma);
y2=distfun_normpdf(2.1,mu,sigma);
assert_checkequal(y,y2);
//
// "cdf" for x=2.1 of Normal(0,1)
fcdfnorm=distfun_name2func("cdf","norm");
mu=0; 
sigma=1;
p=fcdfnorm(2.1,mu,sigma);
p2=distfun_normcdf(2.1,mu,sigma);
assert_checkequal(p,p2);
//
// "rnd" for Normal(0,1)
frndnorm=distfun_name2func("rnd","norm");
mu=0; 
sigma=1;
x=frndnorm(mu,sigma,10,2);
assert_checkequal(size(x),[10,2]);
//
// Check error when function is unknown
purpose="rnd";
shortname="foo";
instr="f=distfun_name2func(purpose,shortname)";
msg=msprintf(gettext("%s: The distribution ""%s"" is unknown."), "distfun_name2func" , "foo" );
assert_checkerror(instr, msg , 10000 );


