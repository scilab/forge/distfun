// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

s=distfun_list("longnames");
assert_checkequal(typeof(s),"string");
assert_checkequal(size(s,"c"),1);
assert_checktrue(or(s=="Normal"));
nbdistrib=size(s,"r"); // Number of distributions

//
s=distfun_list("shortnames");
assert_checkequal(typeof(s),"string");
assert_checkequal(size(s),[nbdistrib,1]);
assert_checktrue(or(s=="norm"));

//
s1=distfun_list("longnames");
s2=distfun_list("shortnames");
mprintf("%-25s : ""%s""\n",[s1,s2]);
