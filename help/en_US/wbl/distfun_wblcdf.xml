<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_wblcdf.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_wblcdf" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_wblcdf</refname>
    <refpurpose>Weibull CDF</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   p = distfun_wblcdf ( x , a , b )
   p = distfun_wblcdf ( x , a , b , lowertail )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles, the outcome, x>=0</para></listitem></varlistentry>
   <varlistentry><term>a :</term>
      <listitem><para> a matrix of doubles, the scale parameter, a>0.</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a matrix of doubles, the shape parameter, b>0.</para></listitem></varlistentry>
   <varlistentry><term>lowertail :</term>
      <listitem><para> a 1-by-1 matrix of booleans, the tail (default lowertail=%t). If lowertail is true (the default), then considers P(X&lt;=x) otherwise P(X&gt;x).</para></listitem></varlistentry>
   <varlistentry><term>p :</term>
      <listitem><para> a matrix of doubles, the probability.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the Weibull cumulated distribution function.
   </para>
   <para>
Any scalar input argument is expanded to a matrix of doubles of the same size as the other input arguments.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Check expansion of a and b
computed = distfun_wblcdf ( 0.1:0.2:0.7 , 2 , 2 )
expected = [0.0024969  0.0222488  0.0605869  0.1152941]

// Check bounds of x
// This generates an error:
// distfun_wblcdf([-1 0 1 2],2,3)

// Plot the function
a = 1;
b = [0.5 1 1.5 5];
cols = [1 2 3 5];
nf = size(cols,"*");
lgd = [];
scf();
for k = 1 : nf
x = linspace(0,2.5,1000);
y = distfun_wblcdf(x,a,b(k));
plot(x,y)
str = msprintf("a=%s, b=%s",..
string(a),string(b(k)));
lgd($+1) = str;
end
h = gcf();
for k = 1 : nf
hk = h.children.children.children(nf - k + 1);
hk.foreground = cols(k);
end
xtitle("Weibull CDF","x","$P(X\leq x)$");
legend(lgd);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
