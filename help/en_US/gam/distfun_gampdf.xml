<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_gampdf.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_gampdf" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_gampdf</refname>
    <refpurpose>Gamma PDF</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   y = distfun_gampdf ( x , a , b )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles, the outcome, x>=0</para></listitem></varlistentry>
   <varlistentry><term>a :</term>
      <listitem><para> a matrix of doubles, the shape parameter, a>0.</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a matrix of doubles, the scale parameter, b>0.</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a matrix of doubles, the density</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the Gamma probability distribution function.
   </para>
   <para>
Any scalar input argument is expanded to a matrix of doubles of the same size as the other input arguments.
   </para>
   <para>
The function definition is:
   </para>
   <para>
<latex>
\begin{eqnarray}
f(x,a,b) = \frac{1}{b^a\Gamma(a)} x^{a-1} \exp\left(-\frac{x}{b}\right)
\end{eqnarray}
</latex>
   </para>
   <para>
Compatibility note.
   </para>
   <para>
This function is compatible with Matlab, but
not compatible with R.
Indeed, notice that b, the scale, is the inverse of the rate.
Other computing languages (including R), use 1/b as the
second parameter of the Gamma distribution.
Hence, the calling sequence
   </para>
   <para>
distfun_gampdf(x,a,b)
   </para>
   <para>
corresponds to the R calling sequence:
   </para>
   <para>
dgamma(x,a,1/b)
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Test x scalar, a scalar, b expanded
b = 1:5;
computed = distfun_gampdf(1,1,b);
expected = [..
3.678794411714423340D-01 , ..
3.032653298563167121D-01 , ..
2.388437701912631272D-01 , ..
1.947001957678512196D-01 , ..
1.637461506155963586D-01 ..
];

// Test all expanded
computed = distfun_gampdf([1 1],[2 2],[3 3]);
expected = [..
7.961459006375437575D-02 , ..
7.961459006375437575D-02 ..
];

// Plot the function
shape = [1 2 3 5 9];
scale = [2 2 2 1 0.5];
cols = [1 2 3 4 5];
nf = size(cols,"*");
lgd = [];
scf();
for k = 1 : nf
x = linspace(0,20,1000);
y = distfun_gampdf ( x , shape(k) , scale(k) );
plot(x,y)
str = msprintf("shape=%s, scale=%s",..
string(shape(k)),string(scale(k)));
lgd($+1) = str;
end
h = gcf();
for k = 1 : nf
hcc = h.children.children;
hcc.children(nf - k + 1).foreground = cols(k);
end
xtitle("Gamma PDF","x","y")
legend(lgd);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 2012 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
