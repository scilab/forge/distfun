<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_histocreate.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_histocreate" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_histocreate</refname>
    <refpurpose>Creates an histogram</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   h=distfun_histocreate("data",data)
   h=distfun_histocreate("data",data,nbins)
   h=distfun_histocreate("data",data,nbins,binmethod)
   h=distfun_histocreate("pdf",edges,heights)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>data :</term>
      <listitem><para> a 1-by-m or m-by-1 matrix of doubles, the observations</para></listitem></varlistentry>
   <varlistentry><term>nbins :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the number of bins (default is Scott rule). If nbins is not a scalar, it contains the edges of the classes, in increasing order.</para></listitem></varlistentry>
   <varlistentry><term>binmethod :</term>
      <listitem><para> a 1-by-1 matrix of strings, the binning method. Must be "scott" (default), "sturges" or "integers".</para></listitem></varlistentry>
   <varlistentry><term>edges :</term>
      <listitem><para> a (nbins+1)-by-1 matrix of doubles, the edges of the classes, in increasing order.</para></listitem></varlistentry>
   <varlistentry><term>heights :</term>
      <listitem><para> a nbins-by-1 matrix of doubles, the height of each class.</para></listitem></varlistentry>
   <varlistentry><term>h :</term>
      <listitem><para> an histogram object</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
distfun_histocreate("data",...) creates the histogram object
associated with the data.
   </para>
   <para>
distfun_histocreate("pdf",...) creates the histogram object
associated with the given edges and heights.
   </para>
   <para>
The h object can then be passed to the distfun_histo*
functions to manage the corresponding histogram.
   </para>
   <para>
If nbins is not provided, compute automatically the number
of bins and the edges from the data.
   </para>
   <para>
<itemizedlist>
<listitem>
<para>
If binmethod=="scott", the bin width is h=3.5*sigma/(m**(1/3)).
This is a good choice for normal data.
</para>
</listitem>
<listitem>
<para>
If binmethod=="sturges", the number of bins is
nbins=ceil(log2(m)+1).
</para>
</listitem>
<listitem>
<para>
If binmethod=="integers", the bins width is 1.
</para>
</listitem>
</itemizedlist>
   </para>
   <para>
The bins are chosen so that
each bin contains at least one observation.
This allows to invert the CDF and prevents errors
from the dsearch function.
   </para>
   <para>
Implementation notes
   </para>
   <para>
The algorithm is the following.
We computes the number of classes and the edges of the
histogram classes, with equal widths.
Then we compute the number of observations in each bin.
The pdf and the cdf of the histogram is then computed
according to the number of observations in each bin.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
m=1000; // Number of observations
data=distfun_normrnd(0,1,m,1);
h=distfun_histocreate("data",data)
// Set number of bins
nbins=5;
h=distfun_histocreate("data",data,nbins)
// Use Sturges rule
h=distfun_histocreate("data",data,[],"sturges")

// Integer data
m=1000; // Number of observations
pr=0.7;
data=distfun_geornd(pr,1,m);
h=distfun_histocreate("data",data,[],"integers")

// Histogram from edges and heights
edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
heights=[0.0314,0.1361,0.3541,0.317,0.098,0.0152];
h=distfun_histocreate("pdf",edges,heights)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
