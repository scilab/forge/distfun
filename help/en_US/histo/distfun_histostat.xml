<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_histostat.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_histostat" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_histostat</refname>
    <refpurpose>Histogram mean and variance</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   M=distfun_histostat(h)
   [M,V]=distfun_histostat(h)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>h :</term>
      <listitem><para> an histogram object</para></listitem></varlistentry>
   <varlistentry><term>M :</term>
      <listitem><para> a matrix of doubles, the mean</para></listitem></varlistentry>
   <varlistentry><term>V :</term>
      <listitem><para> a matrix of doubles, the variance</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes statistics from the histogram.
   </para>
   <para>
The h object must be created with distfun_histocreate.
   </para>
   <para>
The Mean and Variance of the Histogram Distribution are:
   </para>
   <para>
<latex>
\begin{eqnarray}
M &amp;=&amp; \frac{1}{2S} \sum_{i=1}^{nbins} f_i l_i (e_i+e_{i+1}) \\
V &amp;=&amp; \frac{1}{3S} \sum_{i=1}^{nbins} f_i l_i (e_i^2+e_i e_{i+1}+e_{i+1}^2),
\end{eqnarray}
</latex>
   </para>
   <para>
where
   </para>
   <para>
<latex>
S= \sum_{i=1}^{nbins} f_i l_i,
</latex>
   </para>
   <para>
<latex>e_i</latex> are the edges of the bins,
<latex>f_i</latex> are the heights of the histogram and
<latex>l_i=e_{i+1}-e_i</latex> are the lengths of the bins.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
m=1000;
data=distfun_normrnd(0,1,m,1);
h=distfun_histocreate("data",data);
[M,V]=distfun_histostat(h)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2014 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
