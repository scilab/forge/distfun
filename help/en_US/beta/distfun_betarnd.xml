<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_betarnd.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_betarnd" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_betarnd</refname>
    <refpurpose>Beta random numbers</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x = distfun_betarnd ( a , b )
   x = distfun_betarnd ( a , b , [m,n] )
   x = distfun_betarnd ( a , b , m , n )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>a :</term>
      <listitem><para> a matrix of doubles, the first shape parameter, a>=0.</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a matrix of doubles, the first shape parameter, b>=0.</para></listitem></varlistentry>
   <varlistentry><term>m :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the number of rows of x</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the number of columns of x</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para> a matrix of doubles, the random numbers, in the interval [0,1].</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Generates random variables from the Beta distribution.
   </para>
   <para>
Any scalar input argument is expanded to a matrix of doubles of
the same size as the other input arguments.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Use x = distfun_betarnd ( a , b )
x=distfun_betarnd(1:6,(1:6)^-1)
x=distfun_betarnd(1:6,1)
x=distfun_betarnd(1,(1:6)^-1)

// Check x = distfun_betarnd ( a , b , v )
x = distfun_betarnd(2,1,[1 5])
x = distfun_betarnd(2,1,[3 2])

// Use x = distfun_betarnd ( a , b , m , n )
x = distfun_betarnd([1 2 3;4 5 6],0.1,2,3)
x = distfun_betarnd(2,1,2,3)
x = distfun_betarnd(1,[1 2 3;4 5 6],2,3)

// Check mean and variance for x = distfun_betarnd ( a , b )
N = 1000;
a = 1:6;
b = (1:6)^-1;
for i = 1:N
computed(i,1:6) = distfun_betarnd(a,b);
end
[M,V] = distfun_betastat ( a , b )
Mx = mean(computed, "r")
Vx = variance(computed, "r")

// Make a plot of the actual distribution of the numbers
a = 2;
b = 3;
x = distfun_betarnd(a,b,1,1000);
histplot(10,x)
x = linspace(0,1,1000);
y = distfun_betapdf(x,a,b);
plot(x,y)
xtitle("Beta random variables","X","Density");
legend(["Empirical","PDF"]);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - 2014 - Michael Baudin</member>
   <member>Copyright (C) 2009-2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
