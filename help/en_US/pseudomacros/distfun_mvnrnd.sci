// Copyright (C) 2014 - 2015 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x = distfun_mvnrnd ( varargin )
    // Normal random numbers
    //
    // Calling Sequence
    //   x=distfun_mvnrnd(mu,sigma)
    //   x=distfun_mvnrnd(mu,sigma,n)
    //
    // Parameters
    //   mu : a 1-by-d matrix of doubles, the average
    //   sigma : a d-by-d matrix of doubles, the covariance matrix, symmetric, positive definite
    //   n : a 1-by-1 matrix of floating point integers, the number of rows in x (default n=1)
    //   x: a n-by-d matrix of doubles, the random numbers.
    //
    // Description
    //   Generates random variables from the Multivariate Normal distribution function.
    //
    //   Any scalar input argument is expanded to a matrix of doubles of 
    //   the same size as the other input arguments.
    // 
    // Examples
    // mu=[12,-31];
    // sigma = [
    //  3.0  0.5
    //  0.5  1.0
    // ]
    // x=distfun_mvnrnd(mu,sigma,5);
    // disp(x)
    // n=1000;
    // x=distfun_mvnrnd(mu,sigma,n);
    // plot(x(:,1),x(:,2),"b.")
    // xtitle("Multivariate Normal Random Numbers","X1","X2")
    // mean(x,"r") // Must be close to mu
    // cov(x) // Must be close to sigma (requires stixbox)
    //
    // Authors
    //   Copyright (C) 2014 -2015 - Michael Baudin

endfunction

