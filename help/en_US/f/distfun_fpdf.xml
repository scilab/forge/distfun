<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from distfun_fpdf.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="distfun_fpdf" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>distfun_fpdf</refname>
    <refpurpose>F-distribution PDF</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   y = distfun_fpdf(x,v1,v2)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles. x is real and x>=0.</para></listitem></varlistentry>
   <varlistentry><term>v1 :</term>
      <listitem><para> a matrix of doubles, numerator degrees of freedom, v1&gt;0 (can be non integer).</para></listitem></varlistentry>
   <varlistentry><term>v2 :</term>
      <listitem><para> a matrix of doubles, denominator degrees of freedom, v2&gt;0 (can be non integer).</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a matrix of doubles, the probability density.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the probability distribution function of
the f distribution function.
   </para>
   <para>
Any scalar input argument is expanded to a matrix of doubles
of the same size as the other input arguments.
   </para>
   <para>
The F distribution has density
   </para>
   <para>
<latex>
\begin{eqnarray}
f(x) = \frac{\sqrt{\frac{(v_1 x)^{v_1} v_2^{v_2}}{(v_1 x+v_2)^{v_1+v_2}}}}{x B\left(\frac{v_1}{2},\frac{v_2}{2}\right)}
\end{eqnarray}
</latex>
   </para>
   <para>
for x &gt;= 0 and is zero if x&lt;0.
   </para>
   <para>
Analysis of the random variable.
   </para>
   <para>
If R1 is a chi-squared random variable with v1 degrees of freedom and
R2 is a chi-squared random variable with v2 degrees of freedom, therefore
the random variable
   </para>
   <para>
<latex>
\begin{eqnarray}
\frac{R_1/v_1}{R_2/v_2}
\end{eqnarray}
</latex>
   </para>
   <para>
has a F-distribution with parameters v1 and v2.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Test with x scalar, v1 scalar, v2 scalar
computed = distfun_fpdf(3,4,5)
expected = 0.06817955

// Test with x scalar, v1 scalar, v2 scalar
computed = distfun_fpdf(3,2.5,1.5)
expected = 0.0623281

computed = distfun_fpdf(1.e2,1.e10,1.e-10)
expected = 5.000D-13

// Plot the function
h=scf();
x = linspace(0,5,1000);
p1 = distfun_fcdf(x,1,1);
p2 = distfun_fcdf(x,2,1);
p3 = distfun_fcdf(x,5,2);
p4 = distfun_fcdf(x,100,1);
p5 = distfun_fcdf(x,100,100);
plot(x,p1,"r")
plot(x,p2,"g")
plot(x,p3,"b")
plot(x,p4,"y")
plot(x,p5,"k")
legend([
"v1=1, v2=1"
"v1=2, v2=1";
"v1=5, v2=2"
"v1=100, v2=1"
"v1=100, v2=100"
]);
xtitle("F PDF","x","y");

// See how the distribution goes when
// v1 and v2 are large
scf();
x=linspace(0,2,100);
y=distfun_fpdf(x,1.e1,1.e1);
plot(x,y,"r")
y=distfun_fpdf(x,1.e2,1.e2);
plot(x,y,"g")
y=distfun_fpdf(x,1.e3,1.e3);
plot(x,y,"b")
y=distfun_fpdf(x,1.e4,1.e4);
plot(x,y,"k")
xtitle("F PDF","x","Density")
legend([
"v1=10^1, v2=10^1"
"v1=10^2, v2=10^2"
"v1=10^3, v2=10^3"
"v1=10^4, v2=10^4"
]);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/F-distribution</para>
   <para>Catherine Loader, http://svn.r-project.org/R/trunk/src/nmath/df.c</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Prateek Papriwal</member>
   <member>Copyright (C) 2012 - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
