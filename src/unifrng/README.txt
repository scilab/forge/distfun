Uniform Random Number Generation Library

Purpose
-------

This library provides sources to produce uniform random numbers.

Here are the list of files related to the generation of uniform 
random numbers :

    "clcg2.c"    
    "clcg4.c"    
    "fsultra.c"  
    "kiss.c"     
    "mt.c"        
    "unifrng.c"
    "unifrng_phraseToSeed.c"
    "urand.c"

History
-------

This collection was adapted from the "Randlib" module, designed 
by Bruno Pincon.

Authors
-------

 * Copyright (C) 2012 - Michael Baudin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2008 - 2011 - INRIA - Michael Baudin
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
 * Copyright (C) 2005 - Bruno Pincon
 * Copyright (C) 2004 - Bruno Pincon
 * Copyright (C) 2002 - Bruno Pincon
 * Copyright (C) 1997, 1999 - Makoto Matsumoto and Takuji Nishimura
 * Copyright (C) 1999 - G. Marsaglia
 * Copyright (C) 1992 - Arif Zaman
 * Copyright (C) 1992 - George Marsaglia
 * Copyright (C) 1973 - CLEVE B. MOLER
 * Copyright (C) 1973 - MICHAEL A. MALCOLM
 * Copyright (C) Luc Devroye
 * Copyright (C) Pierre Lecuyer
 * Probably many others ...

Licence
------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
