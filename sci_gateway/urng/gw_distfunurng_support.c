// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

#include <limits.h>

#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"

#include "gw_distfunurng_support.h" 
#include "gwsupport.h" 


