// Copyright (C) 2015 - Michael Baudin
// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2012 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// gw_distfunurng.h
//   Header for the DISTFUN urng gateway.
//
#ifndef __SCI_GW_DISTFUNURNG_H__
#define __SCI_GW_DISTFUNURNG_H__

#include "gwsupport.h"

// Uniform Random number generators
int sci_distfun_genget(GW_PARAMETERS);
int sci_distfun_genset(GW_PARAMETERS);
int sci_distfun_seedget(GW_PARAMETERS);
int sci_distfun_seedset(GW_PARAMETERS);

// For clcg4
int sci_distfun_streamset(GW_PARAMETERS);
int sci_distfun_streamget(GW_PARAMETERS);
int sci_distfun_streaminit(GW_PARAMETERS);

#endif /* __SCI_GW_DISTFUNURNG_H__ */
