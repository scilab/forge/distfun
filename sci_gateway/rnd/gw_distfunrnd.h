
// Copyright (C) 2012 - 2015 - Michael Baudin
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// gw_distfunrnd.h
//   Header for the DISTFUN rnd gateway.
//
#ifndef __SCI_GW_DISTFUNGRAND_H__
#define __SCI_GW_DISTFUNGRAND_H__

#include "gwsupport.h"

// Non-Uniform Random number generators
int sci_distfun_rndbeta(GW_PARAMETERS);
int sci_distfun_rndf(GW_PARAMETERS);
int sci_distfun_rndmul(GW_PARAMETERS);
int sci_distfun_rndgam(GW_PARAMETERS);
int sci_distfun_rndnorm(GW_PARAMETERS);
int sci_distfun_rndunf(GW_PARAMETERS);
int sci_distfun_rnduin(GW_PARAMETERS);
int sci_distfun_rndprm(GW_PARAMETERS);
int sci_distfun_rndnbn(GW_PARAMETERS);
int sci_distfun_rndbino(GW_PARAMETERS);
int sci_distfun_rndmn(GW_PARAMETERS);
int sci_distfun_rndmarkov(GW_PARAMETERS);
int sci_distfun_rndnch(GW_PARAMETERS);
int sci_distfun_rndnf(GW_PARAMETERS);
int sci_distfun_rndchi2(GW_PARAMETERS);
int sci_distfun_rndexp(GW_PARAMETERS);
int sci_distfun_rndpoiss(GW_PARAMETERS);
int sci_distfun_rndlogn(GW_PARAMETERS);
int sci_distfun_rndnct(GW_PARAMETERS);
int sci_distfun_rndwbl(GW_PARAMETERS);
int sci_distfun_rndev(GW_PARAMETERS);
int sci_distfun_rndks(GW_PARAMETERS);

#endif /* __SCI_GW_DISTFUNGRAND_H__ */
