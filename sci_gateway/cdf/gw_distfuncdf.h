
// Copyright (C) 2012 - 2015 - Michael Baudin
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// gw_distfuncdf.h
//   Header for the DISTFUN CDF gateway.
//
#ifndef __SCI_GW_DISTFUNCDF_H__
#define __SCI_GW_DISTFUNCDF_H__

#include "gwsupport.h"

// Uniform distribution
int sci_distfun_cdfunif(GW_PARAMETERS);
int sci_distfun_invunif(GW_PARAMETERS);
int sci_distfun_pdfunif(GW_PARAMETERS);

// T distribution
int sci_distfun_cdft(GW_PARAMETERS);
int sci_distfun_invt(GW_PARAMETERS);
int sci_distfun_pdft(GW_PARAMETERS);

// Beta distribution
int sci_distfun_cdfbeta(GW_PARAMETERS);
int sci_distfun_invbeta(GW_PARAMETERS);
int sci_distfun_pdfbeta(GW_PARAMETERS);

// Binomial distribution
int sci_distfun_cdfbino(GW_PARAMETERS);
int sci_distfun_invbino(GW_PARAMETERS);
int sci_distfun_pdfbino(GW_PARAMETERS);

// Chi distribution
int sci_distfun_cdfchi2(GW_PARAMETERS);
int sci_distfun_invchi2(GW_PARAMETERS);
int sci_distfun_pdfchi2(GW_PARAMETERS);
int sci_distfun_cdfncx2(GW_PARAMETERS);

// F distribution
int sci_distfun_cdff(GW_PARAMETERS);
int sci_distfun_invf(GW_PARAMETERS);
int sci_distfun_pdff(GW_PARAMETERS);

// Gamma distribution
int sci_distfun_cdfgam(GW_PARAMETERS);
int sci_distfun_invgam(GW_PARAMETERS);
int sci_distfun_pdfgam(GW_PARAMETERS);

// Normale distribution
int sci_distfun_cdfnorm(GW_PARAMETERS);
int sci_distfun_invnorm(GW_PARAMETERS);
int sci_distfun_pdfnorm(GW_PARAMETERS);

// Poisson distribution
int sci_distfun_cdfpoiss(GW_PARAMETERS);
int sci_distfun_invpoiss(GW_PARAMETERS);
int sci_distfun_pdfpoiss(GW_PARAMETERS);

// Hypergeometric distribution
int sci_distfun_cdfhyge(GW_PARAMETERS);
int sci_distfun_invhyge(GW_PARAMETERS);
int sci_distfun_pdfhyge(GW_PARAMETERS);

// Exponential distribution
int sci_distfun_pdfexp(GW_PARAMETERS);
int sci_distfun_cdfexp(GW_PARAMETERS);
int sci_distfun_invexp(GW_PARAMETERS);

// Geometric distribution
int sci_distfun_pdfgeo(GW_PARAMETERS);
int sci_distfun_cdfgeo(GW_PARAMETERS);
int sci_distfun_invgeo(GW_PARAMETERS);

// Log-Normale distribution
int sci_distfun_pdflogn(GW_PARAMETERS);
int sci_distfun_cdflogn(GW_PARAMETERS);
int sci_distfun_invlogn(GW_PARAMETERS);

// Non-Central Chi distribution
int sci_distfun_invchn(GW_PARAMETERS);
int sci_distfun_cdfchn(GW_PARAMETERS);

// Non-Central F distribution
int sci_distfun_invfnc(GW_PARAMETERS);
int sci_distfun_cdfncf(GW_PARAMETERS);

// Negative Binomiale distribution
int sci_distfun_invnbn(GW_PARAMETERS);
int sci_distfun_cdfnbn(GW_PARAMETERS);
int sci_distfun_pdfnbn(GW_PARAMETERS);

// Noncentral T distribution
int sci_distfun_cdfnct(GW_PARAMETERS);
int sci_distfun_invnct(GW_PARAMETERS);
int sci_distfun_pdfnct(GW_PARAMETERS);

// Multivariate Normal
int sci_distfun_pdfmvn(GW_PARAMETERS);

// Weibull distribution
int sci_distfun_cdfwbl(GW_PARAMETERS);
int sci_distfun_invwbl(GW_PARAMETERS);
int sci_distfun_pdfwbl(GW_PARAMETERS);

// Extreme Value distribution
int sci_distfun_cdfev(GW_PARAMETERS);
int sci_distfun_invev(GW_PARAMETERS);
int sci_distfun_pdfev(GW_PARAMETERS);

// Regularized Incomplete Gamma Function
int sci_distfun_incgamma(GW_PARAMETERS);

// Kolmogorov Smirnov Distribution
int sci_distfun_cdfks(GW_PARAMETERS);
int sci_distfun_invks(GW_PARAMETERS);
int sci_distfun_pdfks(GW_PARAMETERS);

#endif /* __SCI_GW_DISTFUNCDF_H__ */
