
// Copyright (C) 2012 - 2015 - Michael Baudin
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// gw_distfundriver.h
//   Header for the DISTFUN DRIVER gateway.
//
#ifndef __SCI_GW_DISTFUNCDF_H__
#define __SCI_GW_DISTFUNCDF_H__

#include "gwsupport.h"

int sci_distfun_startup(GW_PARAMETERS);
int sci_distfun_verboseset(GW_PARAMETERS);

#endif /* __SCI_GW_DISTFUNCDF_H__ */
