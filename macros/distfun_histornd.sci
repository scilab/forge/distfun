// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function x=distfun_histornd(varargin)
    // Histogram random numbers
    //  
    // Calling Sequence
    //   x=distfun_histornd(h)
    //   x=distfun_histornd(h,[m,n])
    //   x=distfun_histornd(h,m,n)
    //
    // Parameters
    // h : an histogram object
    // m : a 1-by-1 matrix of floating point integers, the number of rows of x
    // n : a 1-by-1 matrix of floating point integers, the number of columns of x
    // x: a matrix of doubles, the random numbers.
    //
    // Description
    // Generates random variables from the Histogram distribution function.
    //
    // The h object must be created with distfun_histocreate.
    //
    // Examples
    // m=1000;
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data);
    // x=distfun_histornd(h,1000,1);
    // scf();
    // histplot(10,x)
    // x = linspace(-4,4);
    // y=distfun_histopdf(x,h);
    // plot(x,y,"b-")
    // legend(["Histogram","Exact"]);
    // xtitle("Histogram")
    // xlabel("x")
    // ylabel("Density")
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    // Load Internals lib
    path = distfun_getpath (  )
    internallib  = lib(fullfile(path,"macros","internals"))

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histornd" , rhs , 2:3 )
    apifun_checklhs ( "distfun_histornd" , lhs , 0:1 )
    //
    h = varargin(1)
    //
    // Check type
    apifun_checktype ( "distfun_histornd" , h , "h" , 1 , "st" )
    if ( rhs == 2 ) then
        v = varargin(2)
    end
    if ( rhs == 3 ) then
        m = varargin(2)
        n = varargin(3)
    end
    //
    // Check size : OK
    //
    // Check content : OK
    //
    // Check v, m, n
    distfun_checkvmn ( "distfun_histornd" , 2 , varargin(2:$) )
    if (rhs==2) then
        m = v(1)
        n = v(2)
    end

    // Proceed...
    u=distfun_unifrnd(0,1,m,n)
    x=distfun_histoinv(u,h)
endfunction
