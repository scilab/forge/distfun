// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function q = distfun_p2q ( p )
  // Returns q = 1-p
  // Manages the special case where p==[].
  // This is because 1-[] is equal to 1, instead of [].
  if ( p == [] ) then
    q = []
  else
    q = 1-p
  end
endfunction

