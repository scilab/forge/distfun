// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function h=distfun_histocreate(varargin)
    // Creates an histogram
    //  
    // Calling Sequence
    //   h=distfun_histocreate("data",data)
    //   h=distfun_histocreate("data",data,nbins)
    //   h=distfun_histocreate("data",data,nbins,binmethod)
    //   h=distfun_histocreate("pdf",edges,heights)
    //             
    // Parameters
    // data : a 1-by-m or m-by-1 matrix of doubles, the observations
    // nbins : a 1-by-1 matrix of doubles, the number of bins (default is Scott rule). If nbins is not a scalar, it contains the edges of the classes, in increasing order.
    // binmethod : a 1-by-1 matrix of strings, the binning method. Must be "scott" (default), "sturges" or "integers".
    // edges : a (nbins+1)-by-1 matrix of doubles, the edges of the classes, in increasing order.
    // heights : a nbins-by-1 matrix of doubles, the height of each class.
    // h : an histogram object
    //
    // Description
    // distfun_histocreate("data",...) creates the histogram object 
    // associated with the data.
    //
    // distfun_histocreate("pdf",...) creates the histogram object 
    // associated with the given edges and heights.
    //
    // The h object can then be passed to the distfun_histo* 
    // functions to manage the corresponding histogram.
    //
    // If nbins is not provided, compute automatically the number 
    // of bins and the edges from the data.
    //
    // <itemizedlist>
    // <listitem>
    // <para>
    // If binmethod=="scott", the bin width is h=3.5*sigma/(m**(1/3)).
    // This is a good choice for normal data.
    // </para>
    // </listitem>
    // <listitem>
    // <para>
    // If binmethod=="sturges", the number of bins is 
    // nbins=ceil(log2(m)+1).
    // </para>
    // </listitem>
    // <listitem>
    // <para>
    // If binmethod=="integers", the bins width is 1. 
    // </para>
    // </listitem>
    // </itemizedlist>
    // 
    // The bins are chosen so that 
    // each bin contains at least one observation. 
    // This allows to invert the CDF and prevents errors 
    // from the dsearch function.
    //
    // Implementation notes
    //
    // The algorithm is the following. 
    // We computes the number of classes and the edges of the 
    // histogram classes, with equal widths. 
    // Then we compute the number of observations in each bin. 
    // The pdf and the cdf of the histogram is then computed 
    // according to the number of observations in each bin. 
    //
    // Examples
    // m=1000; // Number of observations
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data)
    // // Set number of bins
    // nbins=5;
    // h=distfun_histocreate("data",data,nbins)
    // // Use Sturges rule
    // h=distfun_histocreate("data",data,[],"sturges")
    //
    // // Integer data
    // m=1000; // Number of observations
    // pr=0.7;
    // data=distfun_geornd(pr,1,m);
    // h=distfun_histocreate("data",data,[],"integers")
    //
    // // Histogram from edges and heights
    // edges=[-3.028,-1.978,-0.927,0.122,1.173,2.223,3.273];
    // heights=[0.0314,0.1361,0.3541,0.317,0.098,0.0152];
    // h=distfun_histocreate("pdf",edges,heights)
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    function nbins=distfun_histosturges(data)
        m=size(data,"*")
        nbins=ceil(log2(m)+1)
    endfunction

    function nbins=distfun_histoscott(data)
        m=size(data,"*")
        sigma=stdev(data)
        h=3.5*sigma/(m**(1. /3.))
        xmin=min(data)
        xmax=max(data)
        nbins=ceil((xmax-xmin)/h)
        nbins=max(nbins,1)
    endfunction

    function edges=distfun_histoedges(data,nbins)
        // Compute classes
        minx = min(data);
        maxx = max(data);
        if (minx == maxx) then
            minx = minx - floor(nbins/2); 
            maxx = maxx + ceil(nbins/2);
        end
        edges = linspace(minx, maxx, nbins+1)';
    endfunction

    function [nbins,edges,count]=distfun_histoautonbins(data,binmethod)
        // Compute nbins and edges automatically from the data 
        // and binmethod.
        if (binmethod=="sturges") then
            nbins=distfun_histosturges(data)
        else
            nbins=distfun_histoscott(data)
        end
        // Compute edges
        edges=distfun_histoedges(data,nbins)
        // Compute histogram
        [ind , count] = dsearch(data, edges);
    endfunction

    function [nbins,edges,count]=distfun_histoautoint(data)
        // Compute nbins and edges in case of "integers" binmethod
        minx = min(data)
        maxx = max(data)
        nbins=1+maxx-minx
        centers=minx:maxx
        edges=linspace(minx-0.5,maxx+0.5,nbins+1)'
        // Compute histogram
        [ind , count] = dsearch(data, edges);
    endfunction
    function [e,h]=distfun_delemptyclasses(edges,heights)
        // heights=[    0.136    0.133    0.094    0.061];
        // edges=[    162.5    165.     167.5    170.     172.5];
        // [e,h]=cleanemptyclasses(edges,heights)
        // //
        // heights=[    0.136    0.133    0.    0.061];
        // edges=[    162.5    165.     167.5    170.     172.5];
        // [e,h]=cleanemptyclasses(edges,heights)
        // //
        // heights=[    0.136    0.133    0.061    0.];
        // edges=[    162.5    165.     167.5    170.     172.5];
        // [e,h]=cleanemptyclasses(edges,heights)
        // //
        // heights=[    0.    0.136    0.133    0.061];
        // edges=[    162.5    165.     167.5    170.     172.5];
        // [e,h]=cleanemptyclasses(edges,heights)
        // Delete empty classes
        nbins=size(heights,"*")
        // 1. Backward loop
        // Delete empty classes
        e=edges
        h=heights
        for i=nbins:-1:1
            if (h(i)>0) then
                break
            end
            e(i+1) = []
            h(i) = []
            nbins=nbins-1
        end
        // 2. Forward loop
        j=1
        for i=1:nbins
            if (h(j)==0) then
                e(j) = []
                h(j) = []
                nbins=nbins-1
                j=j-1
            end
            j=j+1
        end
        // 3. Normalize, again
        w=e(2:$)-e(1:$-1)
        h=h/sum(h.*w)
    endfunction

    function h = distfun_histofromdata(varargin)
        // Creates a new histogram from data.
        //
        // Calling Sequence
        //   h=distfun_histocreate("data",data)
        //   h=distfun_histocreate("data",data,nbins)
        //   h=distfun_histocreate("data",data,nbins,binmethod)
        [lhs,rhs]=argn()
        apifun_checkrhs ( "distfun_histocreate" , rhs , 2:4)
        apifun_checklhs ( "distfun_histocreate" , lhs , 0:1 )
        //
        data=varargin ( 2 );
        nbins=apifun_argindefault(varargin,3,[])
        binmethod=apifun_argindefault(varargin,4,"scott")
        //
        // Check Type
        apifun_checktype ( "distfun_histocreate" , data , "data" , 2 , "constant" )
        apifun_checktype ( "distfun_histocreate" , nbins , "nbins" , 3 , "constant" )
        apifun_checktype ( "distfun_histocreate" , binmethod , "binmethod" , 4 , "string" )
        //
        // Check Size
        apifun_checkvector ( "distfun_histocreate" , data , "data" , 2 )
        if (nbins<>[]) then
            apifun_checkvector ( "distfun_histocreate" , nbins , "nbins" , 3 )
        end
        apifun_checkscalar ( "distfun_histocreate" , binmethod , "binmethod" , 4 )

        //
        // Check content
        avail=["sturges","integers","scott"]
        apifun_checkoption( "distfun_histocreate" , binmethod , "binmethod" , 4, avail)
        //
        // Proceed...
        data=data(:);
        // Compute nbins and edges and count
        if (size(nbins,"*")>1) then
            // nbins represents the edges.
            edges = matrix(nbins,-1,1)   // force row form
            if (min(diff(edges)) <= 0) then

                error(msprintf(gettext("%s: Wrong values for input argument #%d: Elements must be in increasing order.\n"),"distfun_histocreate",3))
            end
            nbins = length(edges)-1
            // Compute count from edges
            [ind , count] = dsearch(data, edges);
        elseif (size(nbins,"*")==1) then
            // nbins is given : use it.
            apifun_checkgreq( "distfun_histocreate" , nbins , "nbins" , 3 , 1)
            apifun_checkflint( "distfun_histocreate" , nbins , "nbins" , 3 )
            // Compute edges from data
            if (binmethod=="integers") then
                minx = min(data)
                maxx = max(data)
                if (minx==maxx) then
                    minx=minx-0.5
                    maxx=maxx-0.5
                end
                edges=linspace(minx-0.5,maxx+0.5,nbins+1)'
            else
                edges=distfun_histoedges(data,nbins)
            end
            // Compute count from edges
            [ind , count] = dsearch(data, edges);
        elseif (binmethod=="integers") then
            [nbins,edges,count]=distfun_histoautoint(data)
        else
            // nbins is not provided : compute it automatically
            [nbins,edges,count]=distfun_histoautonbins(data)
        end
        // Scale for pdf : integral is 1
        m=size(data,"*")
        binwidths=edges(2:$)-edges(1:$-1)
        hpdf=count./(m*binwidths)
        hpdf=hpdf/sum(hpdf.*binwidths)
        // Create object
        h=distfun_histonew(edges,hpdf)
    endfunction

    function h = distfun_histofrompdf(varargin)
        // Creates a new histogram from edges and heights.
        //
        // Calling Sequence
        //   h=distfun_histocreate("pdf",edges,heights)
        [lhs,rhs]=argn()
        apifun_checkrhs ( "distfun_histocreate" , rhs , 3:3)
        apifun_checklhs ( "distfun_histocreate" , lhs , 0:1 )
        //
        edges=varargin ( 2 );
        heights=varargin ( 3 );
        //
        // Check Type
        apifun_checktype ( "distfun_histocreate" , edges , "edges" , 2 , "constant" )
        apifun_checktype ( "distfun_histocreate" , heights , "pdf" , 3 , "constant" )
        //
        // Check Size
        nbins=size(heights,"*")
        apifun_checkvector ( "distfun_histocreate" , edges , "edges" , 2 , nbins+1)
        apifun_checkvector ( "distfun_histocreate" , heights , "pdf" , 3 , nbins)
        //
        // Check content
        if (min(diff(edges)) <= 0) then
            error(msprintf(gettext("%s: Wrong values for input argument #%d: Elements must be in increasing order.\n"),"distfun_histocreate",2))
        end
        apifun_checkgreq( "distfun_histocreate" , heights , "pdf" , 3, 0.)
        //
        edges=edges(:);
        heights=heights(:);
        //
        // Proceed...
        binwidths=edges(2:$)-edges(1:$-1)
        hpdf=heights/sum(heights.*binwidths)
        // Create object
        h=distfun_histonew(edges,hpdf)

    endfunction
    function h=distfun_histonew(edges,hpdf)
        // Creates a new histogram
        // Delete empty classes
        [edges,hpdf]=distfun_delemptyclasses(edges,hpdf)
        nbins=size(hpdf,"*")
        // Scale for cdf
        hcdf=cumsum(hpdf)
        hcdf=hcdf/hcdf($)
        h=struct(..
        "nbins",nbins, ..
        "edges",edges,..
        "pdf",hpdf,..
        "cdf",hcdf)
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histocreate" , rhs , 2:4 )
    apifun_checklhs ( "distfun_histocreate" , lhs , 0:1 )
    //
    action=varargin(1);
    //
    // Check Type, Size, Content
    apifun_checktype ( "distfun_histocreate" , action , "action" , 1 , "string" )
    apifun_checkscalar( "distfun_histocreate" , action , "action" , 1 )
    avail=["data","pdf"]
    apifun_checkoption( "distfun_histocreate" , action , "action" , 1, avail)
    //
    if (action=="data") then
        h = distfun_histofromdata(varargin(1:$))
    else
        h = distfun_histofrompdf(varargin(1:$))
    end

endfunction

