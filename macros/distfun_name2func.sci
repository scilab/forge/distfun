function f=distfun_name2func(varargin)
    // Returns the distribution function from its name
    //
    // Calling Sequence
    //   f=distfun_name2func("inv",shortname)
    //   f=distfun_name2func("pdf",shortname)
    //   f=distfun_name2func("rnd",shortname)
    //   f=distfun_name2func("cdf",shortname)
    //   f=distfun_name2func("stat",shortname)
    //
    // Parameters
    //   shortname : a 1-by-1 matrix of strings, the short name of the distribution
    //   f : the function corresponding to the given shortname
    //
    // Description
    // Returns the distribution function from its name.
    //
    // distfun_name2func("inv",shortname) returns 
    // the quantile function corresponding to the 
    // given short name e.g. : distfun_unifpdf if shortname="unif"
    //
    // distfun_name2func("pdf",shortname) returns 
    // the pdf function.
    //
    // distfun_name2func("cdf",shortname) returns 
    // the cdf function.
    //
    // distfun_name2func("rnd",shortname) returns 
    // the rnd function.
    //
    // distfun_name2func("stat",shortname) returns 
    // the stat function.
    //
    // Examples
    // f=distfun_name2func("inv","norm")
    // mu=0; sigma=1
    // // quantile for p=0.7 of Normal(0,1)
    // f(0.7,mu,sigma) // same as :
    // distfun_norminv(0.7,mu,sigma)
    //
    // Authors
    //   Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_name2func" , rhs , 2:2 )
    apifun_checklhs ( "distfun_name2func" , lhs , 0:1 )
    //
    purpose = varargin(1)
    shortname=varargin(2)
    //
    // Check type
    apifun_checktype ( "distfun_name2func" , purpose , "purpose" , 1 , "string" )
    apifun_checktype ( "distfun_name2func" , shortname , "shortname" , 2 , "string" )
    //
    // Check size
    apifun_checkscalar ( "distfun_name2func" , purpose , "purpose" , 1 )
    apifun_checkscalar ( "distfun_name2func" , shortname , "shortname" , 2 )
    //
    // Check content
    alloptions=[
    "inv"
    "pdf"
    "cdf"
    "rnd"
    "stat"
    ]
    apifun_checkoption( "distfun_name2func" , purpose , "purpose" , 1 , alloptions)
    //
    // Proceed...
    allshortnames=distfun_list("shortnames")
    if(or(allshortnames==shortname)) then
        execstr("f="+"distfun_"+shortname+purpose)
    else
        errmsg = msprintf(gettext("%s: The distribution ""%s"" is unknown."), "distfun_name2func" , shortname );
        error(errmsg)
    end
endfunction
